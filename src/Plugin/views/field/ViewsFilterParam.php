<?php

namespace Drupal\views_filter_param\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * Defines a views filter param form element.
 *
 * @ViewsField("filter_param_views_field")
 */
class ViewsFilterParam extends FieldPluginBase {

  /**
   * Constructs a new BulkForm object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $request_stack);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['views_filter_param_help'] = [
      '#markup' => $this->t("Markup will be added to each row of the table."),
    ];
    parent::buildOptionsForm($form, $form_state);

    $form['include_exclude']['#access'] = FALSE;
    $form['selected_actions']['#access'] = FALSE;
    // $form['exclude']['#access'] = FALSE;
    $form['alter']['#access'] = FALSE;
    $form['empty_field_behavior']['#access'] = FALSE;
    $form['empty']['#access'] = FALSE;
    $form['empty_zero']['#access'] = FALSE;
    $form['hide_empty']['#access'] = FALSE;
    $form['hide_alter_empty']['#access'] = FALSE;

  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $current_path = \Drupal::service('path.current')->getPath();
    $query = \Drupal::request()->query->all();
    foreach ($query as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $val) {
          $query_arr[] = $key . '[]=' . $val;
        }
      }
      else {
        $query_arr[] = $key . '=' . $value;
      }
    }
    $param = implode('&', $query_arr);
    $new_param = '?destination-link=' . $current_path;
    if (!empty($param)) {
      $params = $new_param . '&' . $param;
    }
    else {
      $params = $new_param;
    }
    return $params;
  }

}
