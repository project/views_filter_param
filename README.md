INTRODUCTION
------------

This module provides enabled filters on Views pages.


REQUIREMENTS
-------------

Views, Views UI, Filter, Node, System module enabled.


INSTALLATION
------------

Enable this module from extend list page /admin/modules.


CONFIGURATION
-------------

1) Activate Views filter param module at /admin/modules.
2) Create a new view
    - Goto '/admin/structure/views/add' on your site.
    - Check off 'Create a page'.
    - Check off 'Create a block'.
    - Set the 'Display format' for the page to what you desire.
    - Set the "'Display format' of" to fields.
    - Fill in the rest of the views information.
    - Click Save & edit button.
3) Under the "FIELDS" section, do you see "Content: Views filter param"?  If you do not:
    - Click 'add' button at the "Fields" section and choose field
    "Content:Views filter param", add and set exclude from display and apply.
4) Add the "Edit/Delete link field" Field:
    - Click Add button at the "FIELDS" section.
    - On the rewrite result/Custom text field we can see {{ filter_views_field }}
    available option. Include that option in custom text field.
5) Save the view and you're done.
